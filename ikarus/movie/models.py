from django.db import models
from django.urls import reverse

# Create your models here.
class Monu(models.Model):
    serial=models.CharField(max_length=10,null=True)

    Character=models.CharField(max_length=250,blank=True)
    Dialogue= models.CharField(max_length=500,blank=True)
    EpisodeNo=models.CharField(max_length=250,null=True)
    SEID=models.CharField(max_length=250,blank=True,null=True)
    Season=models.CharField(max_length=250,null=True)
