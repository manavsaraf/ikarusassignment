from django.urls import path,include
from django.conf.urls import url
from . import views
app_name='movie'

urlpatterns = [
    url(r'^\w+(,\w)*/$',views.detail,name='detail'),
]
