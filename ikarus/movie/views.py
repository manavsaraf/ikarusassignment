from django.shortcuts import render
from movie.models import Monu
def index(request):
    stuff=Monu.objects.all().order_by().values('Season').distinct()

    return render(request,'movie/index.html',context={'stuff':stuff})

def detail(request,first_name,last_name):

    nme=first_name +" "+ last_name

    res=Monu.objects.filter(Season=nme)
    return render(request,'movie/detail.html',{'res':res})

def wholeinfo(request,season_name1,season_name2):
    seasonname=season_name1+" "+season_name2
    ans=Monu.objects.filter(EpisodeNo=seasonname)
    return render(request,'movie/wholeinfo.html',{'ans':ans})
