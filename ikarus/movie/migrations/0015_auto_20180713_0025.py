# Generated by Django 2.0.7 on 2018-07-12 18:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movie', '0014_remove_monu_episode'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='monu',
            name='air_date',
        ),
        migrations.RemoveField(
            model_name='monu',
            name='director',
        ),
        migrations.RemoveField(
            model_name='monu',
            name='title',
        ),
        migrations.RemoveField(
            model_name='monu',
            name='writer',
        ),
        migrations.AddField(
            model_name='monu',
            name='Character',
            field=models.CharField(blank=True, max_length=250),
        ),
        migrations.AddField(
            model_name='monu',
            name='Dialogue',
            field=models.CharField(blank=True, max_length=500),
        ),
        migrations.AddField(
            model_name='monu',
            name='EpisodeNo',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='monu',
            name='SEID',
            field=models.CharField(blank=True, max_length=250),
        ),
        migrations.AddField(
            model_name='monu',
            name='Season',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='monu',
            name='serial',
            field=models.IntegerField(null=True),
        ),
    ]
