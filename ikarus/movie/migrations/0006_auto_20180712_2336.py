# Generated by Django 2.0.7 on 2018-07-12 17:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movie', '0005_monu_seasonid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='monu',
            name='seasonid',
            field=models.CharField(blank=True, max_length=250, null=True),
        ),
    ]
