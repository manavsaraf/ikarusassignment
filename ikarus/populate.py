from django.db import models
import os,csv,sys
os.environ.setdefault('DJANGO_SETTINGS_MODULE','ikarus.settings')


import django
django.setup()

from movie.models import Monu

data=csv.reader(open("episode_info.csv"),delimiter=",")
for row in data:
    if row[0]!='serial':
        epsde=Monu()
        epsde.serial=row[0]
        epsde.Character=row[1]
        epsde.Dialogue=row[2]
        epsde.EpisodeNo=row[3]
        epsde.SEID=row[4]
        epsde.Season=row[5]
        epsde.save()
